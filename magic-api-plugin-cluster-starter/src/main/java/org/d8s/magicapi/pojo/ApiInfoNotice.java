package org.d8s.magicapi.pojo;

import lombok.AllArgsConstructor;

/**
 * @author wangshuai@e6yun.com
 * @date 5/24/2021 4:04 PM
 **/

public class ApiInfoNotice {
    private String instanceId;
    private String params;
    private String method;
    private String updateTime;

    public ApiInfoNotice() {
    }

    public ApiInfoNotice(String instanceId, String params, String method, String updateTime) {
        this.instanceId = instanceId;
        this.params = params;
        this.method = method;
        this.updateTime = updateTime;
    }

    public static ApiInfoNoticeBuilder builder() {
        return new ApiInfoNoticeBuilder();
    }

    public String getInstanceId() {
        return this.instanceId;
    }

    public String getParams() {
        return this.params;
    }

    public String getMethod() {
        return this.method;
    }

    public String getUpdateTime() {
        return this.updateTime;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }



    protected boolean canEqual(final Object other) {
        return other instanceof ApiInfoNotice;
    }


    public static class ApiInfoNoticeBuilder {
        private String instanceId;
        private String params;
        private String method;
        private String updateTime;

        ApiInfoNoticeBuilder() {
        }

        public ApiInfoNoticeBuilder instanceId(String instanceId) {
            this.instanceId = instanceId;
            return this;
        }

        public ApiInfoNoticeBuilder params(String params) {
            this.params = params;
            return this;
        }

        public ApiInfoNoticeBuilder method(String method) {
            this.method = method;
            return this;
        }

        public ApiInfoNoticeBuilder updateTime(String updateTime) {
            this.updateTime = updateTime;
            return this;
        }

        public ApiInfoNotice build() {
            return new ApiInfoNotice(instanceId, params, method, updateTime);
        }

    }

    @Override
    public String toString() {
        return "ApiInfoNotice{" +
                "instanceId='" + instanceId + '\'' +
                ", params='" + params + '\'' +
                ", method='" + method + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
